
public class Inicio {
	
	public static void main(String[] args) {
		
				Funcionario f1 = new Funcionario();
				f1.setNome("Jo�o");
				f1.setSobrenome("Pedro");
				f1.setCargo("Programador");
				f1.setSalario(4200.15);
				
				Funcionario f2 = new Funcionario();
				f2.setNome("Jos�");
				f2.setSobrenome("Gomes");
				f2.setCargo("Programador");
				f2.setSalario(5000.00);
				
				Funcionario f3 = new Funcionario();
				f3.setNome("Pedro");
				f3.setSobrenome ("Silva");
				f3.setCargo("Programador");
				f3.setSalario(4500.00);
				
				System.out.println(f1.getNome() +" "+ f1.getSobrenome() + " - " + f1.getCargo() +" - "+ f1.getSalario());
				System.out.println(f2.getNome() +" "+ f2.getSobrenome() + " - " + f2.getCargo()+" - "+ f2.getSalario());
				System.out.println(f3.getNome() +" "+ f3.getSobrenome() + " - " + f3.getCargo() +" - "+ f3.getSalario());
	}

}
