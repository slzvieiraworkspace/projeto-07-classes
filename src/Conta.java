
public class Conta {

	public int numero;
	public String nome;
	private double saldo;
	
	public void depositar(double valor) {
		saldo += valor;
	}
	
	public void retirar(double valor) {
		if (valor <= saldo) {
			saldo -= valor;
		} else {
			System.out.println("Saldo insuficiente");
		}
	}
	
	public double getSaldo() {
		return saldo;
	}
}
