
public class Principal {

	public static void main(String[] args) {
		
		Conta c1;
		c1 = new Conta();
		
		Conta c2 = new Conta();
		
		c1.numero = 5003;
		c1.nome = "Manuel";
		c1.depositar(800.0);
		
		c2.numero = 5004;
		c2.nome = "Joaquim";
		c2.depositar(650.0);
		
		System.out.println(c1.nome);
		System.out.println(c2.getSaldo());
	}
}
