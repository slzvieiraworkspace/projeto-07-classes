
public class Funcionario {
	
		private String nome ;
		private String sobrenome ;
		private String cargo ;
		private double salario ;

		
		public String getNome() {
			return nome;
		}
		public void setNome(String n) {
			nome = n;
		}
	
		public String getSobrenome () {
			return sobrenome;
		}
		public void setSobrenome(String s) {
			sobrenome = s;
		}
		
		public String getCargo() {
			return cargo;
		}
		public void setCargo(String c) {
			cargo = c;
		}
		
		public double getSalario() {
			return salario;
		}
		public void setSalario(double sal) {
			salario = sal;
		}
		
}
